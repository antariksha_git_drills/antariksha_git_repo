import { Octokit, App } from "https://esm.sh/octokit";
const accessToken = "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"; // Replace with your GitHub personal access token
const octokit = new Octokit({ auth: accessToken });

async function createRepo() {
    const repoName = document.getElementById('createRepoName').value;
    try {
        await octokit.request('POST /user/repos', {
        name: repoName,
        headers: {
        'X-GitHub-Api-Version': '2022-11-28'
        }
    })
        console.log('Repository created successfully:', repoName);
        listRepos();
    } catch (error) {
        console.error('Error creating repository:', error);
    }
}

async function listRepos() {
    try {
        const response = await octokit.rest.repos.listForAuthenticatedUser();
        const repositories = response.data;
        const repoList = document.getElementById('repoList');
        repoList.innerHTML = '';
        if (repositories.length > 0) {
            repositories.forEach((repo) => {
                const listItem = document.createElement('li');
                listItem.textContent = repo.name;
                repoList.appendChild(listItem);
            });
        } else {
            repoList.innerHTML = '<li>No repositories found.</li>';
        }
    } catch (error) {
        console.error('Error listing repositories:', error);
    }
}

async function updateRepo() {
    const repoName = document.getElementById('updateRepoName').value;
    const newDescription = document.getElementById('newDescription').value;
    try {
        await octokit.request('PATCH /repos/{owner}/{repo}', {
        owner: 'antman1501',
        repo: repoName,
        name: newDescription,
        headers: {
        'X-GitHub-Api-Version': '2022-11-28'
        }
    })
        console.log('Repository description updated successfully:', newDescription);
        repoName.innerText=""
        listRepos();
    } catch (error) {
        console.error('Error updating repository description:', error);
    }
}

async function deleteRepo() {
    const repoName = document.getElementById('deleteRepoName').value;
    try {
        await octokit.request('DELETE /repos/{owner}/{repo}', {
        owner: 'antman1501',
        repo: repoName,
        headers: {
            'X-GitHub-Api-Version': '2022-11-28'
        }
        })
        console.log('Repository deleted successfully.');
        listRepos();
    } catch (error) {
        console.error('Error deleting repository:', error);
    }
}
listRepos();
document.querySelector('#creation').addEventListener('click',e=>{
    e.preventDefault()
    createRepo();  
})
document.querySelector('#update').addEventListener('click',e=>{
    e.preventDefault()
    updateRepo();  
})
document.querySelector('#deletion').addEventListener('click',e=>{
    e.preventDefault()
    deleteRepo();  
})

